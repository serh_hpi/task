/*
Write a program that prints the balance of an account after the first, second, and
third year. The account has an initial balance of $1,000 and earns 5 percent interest
per year.
*/
package lesson1.introduction;

class P1_04 {
    public static void main(String[] args) {
        double balans=1000;
        double persent=0.05;

        for (int i=1; i<=3; i++) {
            balans+=balans*persent;
            System.out.println("Баланс за " +i+" год="+balans);
        }

    }

}
