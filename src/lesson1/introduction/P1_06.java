/*
Write a program that prints your name in large letters, such as
*    *    **    ****    ****    *   *
*    *   *  *   *   *   *   *   *   *
******  *    *  ****    ****     * *
*    *  ******  *   *   *   *     *
*    *  *    *  *    *  *    *    *
*/
package lesson1.introduction;

class P1_06 {
    public static void main(String[] args) {

        System.out.println(" ****    *******  *******      *******    ********  *          *");
        System.out.println("*    *   *        *      *    *       *   *           *      *");
        System.out.println("*        *        *      *   *            *             *  *");
        System.out.println("  *      *******  ******     *            ********        *");
        System.out.println("    *    *        *    *     *      ***   *               *");
        System.out.println("*     *  *        *     *     *      *    *               *");
        System.out.println(" *****   *******  *      *     ******     ********        *");
    }
}