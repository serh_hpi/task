/*Write a program that prints a face similar to (but different from) the following:
  /////
 +"""""+
(| o o |)
 |  ^  |
 | ‘-’ |
 +-----+
*/
package lesson1.introduction;

class P1_07 {
    public static void main(String[] args) {

        System.out.println("  |||||||||");
        System.out.println("  *********");
        System.out.println("  *       *");
        System.out.println("+-* o   o *-+");
        System.out.println("  *   ^   *");
        System.out.println("  *  шшш  *");
        System.out.println("  *********");
    }
}