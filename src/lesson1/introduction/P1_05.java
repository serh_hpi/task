/*
Write a program that displays your name inside a box on the screen, like this: Dave
Do your best to approximate lines with characters such as |- + .
*/

package lesson1.introduction;

class P1_05 {
    public static void main(String[] args) {
        System.out.println("+--------+");
        System.out.println("| Sergey |");
        System.out.println("+--------+");

    }

}
