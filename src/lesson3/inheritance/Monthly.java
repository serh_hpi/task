package lesson3.inheritance;


import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Monthly extends Appointment {



    public Monthly(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }


  @Override
  public boolean occursOn(int year, int month, int day) throws ParseException {

      String date_ = new SimpleDateFormat("yyyy-MM-dd").format(super.getDate());
      String[] dateParts = String.valueOf(date_).split("-");

      int month_ = Integer.parseInt(dateParts[1]);
      int year_ = Integer.parseInt(dateParts[0]);

      return year == year_ & month == month_;

  }
}
