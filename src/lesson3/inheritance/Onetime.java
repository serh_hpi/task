package lesson3.inheritance;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class Onetime extends Appointment {

    public Onetime(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }

     @Override
     public boolean occursOn(int year, int month, int day) throws ParseException {
         DateFormat dateFormatter = new SimpleDateFormat(String.format("yyyy-MM-dd"));
         return super.getDate().equals(dateFormatter.parse(String.format("%d-%d-%d", year, month, day)));

    }

//..


}
