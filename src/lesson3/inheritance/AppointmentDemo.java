package lesson3.inheritance;


import java.text.ParseException;
import java.util.Scanner;

public class AppointmentDemo {

    public static void main(String[] args) throws ParseException {

        Appointment[] appoint = new Appointment[3];
        appoint[0] = new Onetime("Посетить врача", 2019, 5, 2);
        appoint[1] = new Monthly("Заплатить коммуналку", 2019, 5, 2);
        appoint[2] = new Daily("Получить зарплату", 2019, 5, 2);


        Scanner in = new Scanner(System.in);
        System.out.print("Введите дату поиска в фотмате yyyy-MM-dd: ");
        String date_ = in.nextLine();

        String[] dateParts = date_.split("-");
        int day = Integer.parseInt(dateParts[2]);
        int month = Integer.parseInt(dateParts[1]);
        int year = Integer.parseInt(dateParts[0]);

        for (Appointment appointment : appoint) {
            if (appointment.occursOn(year, month, day)) {
                System.out.print(appointment+" - ");  appointment.print();
            }
        }

    }
}