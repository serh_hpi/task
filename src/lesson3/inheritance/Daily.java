package lesson3.inheritance;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Daily extends Appointment {

    public Daily(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);

    }

    @Override
    public boolean occursOn(int year, int month, int day) throws ParseException {
        String date_ = new SimpleDateFormat("yyyy-MM-dd").format(super.getDate());
        String[] dateParts = String.valueOf(date_).split("-");

        int day_ = Integer.parseInt(dateParts[2]);

        return  day == day_;
    }
}
