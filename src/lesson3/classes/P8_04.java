/*Implement a class Address . An address has a house number, a street, an optional
apartment number, a city, a state, and a postal code. Supply two constructors: one
with an apartment number and one without. Supply a print method that prints the
address with the street on one line and the city, state, and zip code on the next line.
Supply a method public boolean comesBefore(Address other) that tests whether this
address comes before another when the addresses are compared by postal code.*/
package lesson3.classes;

class Address {
    private int Postal_code;
    private String State;
    private String City;
    private String Street;
    private String House;
    private String Apartment;


    public Address(int postal_code, String state, String city, String street, String house, String apartment) {
        Postal_code = postal_code;
        State = state;
        City = city;
        Street = street;
        House = house;
        Apartment = apartment;
    }

    public Address(int postal_code, String state, String city, String street, String house) {
        Postal_code = postal_code;
        State = state;
        City = city;
        Street = street;
        House = house;
        Apartment = "";
    }

    public void print() {
        System.out.println(this.Street + " " + this.House);
        System.out.println(this.City + " " + this.State + " " + this.Postal_code);
    }

    public boolean comesBefore(Address other) {
        if (this.Postal_code< other.Postal_code)  return true;
        return false;
    }

}

public class P8_04 {
    public static void main(String[] args) {
        Address first=new Address(685000, "NW","NewYork","2-st Avenu","2");
        Address second=new Address(690007, "Orange","LA","1-st Avenu","90210");
        System.out.println(first.comesBefore(second));
    }



}

