/*Implement a class SodaCan with methods getSurfaceArea() and get­­Volume().
In the constructor, supply the height and radius of the can.*/

package lesson3.classes;

class SodaCan {
   private double Height;
   private double Radius;

    public SodaCan(double height, double radius ) {
        Height=height;
        Radius=radius;
    }

    public double getSurfaceArea() {
        return 2*Math.PI*this.Radius*(this.Height+this.Radius);
    }

    public double get_Volume() {
       return Math.PI*Math.pow(this.Radius,2)*this.Height;
    }


}

public class P8_05 {
    public static void main(String[] args) {
        SodaCan can=new SodaCan(10,5);
        System.out.println("SurfaceArea="+can.getSurfaceArea());
        System.out.println("Volume="+can.get_Volume());

    }
}
