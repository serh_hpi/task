package lesson3.polymorphism;

/* P9.4    Add a class AnyCorrectChoiceQuestion to the question hierarchy of Section 9.1 that
allows multiple correct choices. The respondent should provide any one of the correct choices.
The answer string should contain all of the correct choices, separated by spaces. 
Provide instructions in the question text.*/

import java.util.Scanner;

public class AnyCorrectChoiceQuestion extends ChoiceQuestion {
    private String allAnswers;

    public AnyCorrectChoiceQuestion() {
        super();
        this.allAnswers="";
   }

    @Override
    public void setAnswer(String correctResponse) {
       if (this.allAnswers.length()==0) {
        this.allAnswers+=correctResponse;
       }
       else this.allAnswers+=" "+correctResponse;
    }

    @Override
    public boolean checkAnswer(String response) {
        Scanner parser = new Scanner(response);
        while (parser.hasNext()) {
            String answer = parser.next();
             if (this.allAnswers.contains(answer)) return true;
        }
        return false;
    }


}
