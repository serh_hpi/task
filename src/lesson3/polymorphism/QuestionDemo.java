package lesson3.polymorphism;

import java.util.Scanner;

/**
   This program shows a simple quiz with two question types.
*/
public class QuestionDemo
{
   public static void main(String[] args)
   {

      Question first = new Question();
      first.setText("Who was the inventor of Java?");
      first.setAnswer("James Gosling");      

      ChoiceQuestion second = new ChoiceQuestion();
      second.setText("In which country was the inventor of Java born?");
      second.addChoice("Australia", false);
      second.addChoice("Canada", true);
      second.addChoice("Denmark", false);
      second.addChoice("United States", false);

      NumericQuestion numquestion = new NumericQuestion();
      numquestion.setText("Постчитайте 0.255-0.254?");
      numquestion.setAnswer("0.001");

      FillInQuestion fillInquestion = new FillInQuestion();
      fillInquestion.setText("The inventor of Java was _James Gosling_");

      MultiChoiceQuestion multiChoicequestion=new MultiChoiceQuestion();
      multiChoicequestion.setText("Каим станам соответсвует часовой пояс новосибирска? Выберите все правильные ответы?");
      multiChoicequestion.addChoice("Россия", true);
      multiChoicequestion.addChoice("Канада", false);
      multiChoicequestion.addChoice("Тайланд", true);
      multiChoicequestion.addChoice("Китай", false);

      AnyCorrectChoiceQuestion anycorrectchoicequestion=new AnyCorrectChoiceQuestion();
      anycorrectchoicequestion.setText("Каим станам соответсвует часовой пояс новосибирска? Выберите любой из правильных ответов?");
      anycorrectchoicequestion.addChoice("Россия", true);
      anycorrectchoicequestion.addChoice("Канада", false);
      anycorrectchoicequestion.addChoice("Тайланд", true);
      anycorrectchoicequestion.addChoice("Китай", false);

      presentQuestion(first);
      presentQuestion(second);
      presentQuestion(numquestion);
      presentQuestion(fillInquestion);
      presentQuestion(multiChoicequestion);
      presentQuestion(anycorrectchoicequestion);
   }

   /**
      Presents a question to the user and checks the response.
      @param q the question
   */
   public static void presentQuestion(Question q)
   {
      q.display();
      System.out.print("Your answer: ");
      Scanner in = new Scanner(System.in);
      String response = in.nextLine();
      System.out.println(q.checkAnswer(response));
   }
}

