/*Write a method that reverses the sequence of elements in an array. For example, if
you call the method with the array
1  4  9  16  9  7  4  9  11
then the array is changed to
11  9  4  7  9  16  9  4  1*/
package lesson2.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class P6_07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d element of array: ", i);
            array[i] = input.nextInt();
        }
        input.close();

        System.out.println("Sorted down array "+Arrays.toString(down_sort(array)));
    }


public static int[] down_sort(int[] array){
       Arrays.sort (array);
       int[] array_2= new int[array.length];

       System.out.println("Original array "+Arrays.toString( array));
        int b=0;
        for (int i = array.length - 1; i >= 0; i--) {
            array_2[b]=array[i];
            b++;
        }
        System.arraycopy(array_2,0,array,0,array_2.length);
        return array;
    }

}

