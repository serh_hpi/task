/*Compute the alternating sum of all elements in an array. For example, if your pro­gram
reads the input then it computes
1  4  9  16  9  7  4  9  11
1 – 4 + 9 – 16 + 9 – 7 + 4 – 9 + 11 = –2*/

package lesson2.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class P6_06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] anArray = new int[10];
        for (int i = 0; i < anArray.length; i++) {
            System.out.printf("%d element of array: ", i);
            anArray[i] = input.nextInt();
        }
        input.close();

        int sum=anArray[0];
        String text= ""+anArray[0];
        for (int i=0; i< anArray.length; i++){
            if (i==1 || i==3 ||i==5 || i==7|| i==9) {
                sum+=anArray[i];
                text+="+"+anArray[i];
            }
            if ( i==2 || i==4 ||i==6 || i==8) {
                sum-=anArray[i];
                text+="-"+anArray[i];
            }
        }
        System.out.println("alternating sum of all elements : "+text+"="+sum);
    }
}

