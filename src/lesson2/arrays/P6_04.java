/*Write a method sumWithoutSmallest that computes the sum of an array of values,
except for the smallest one, in a single loop. In the loop, update the sum and the
smallest value. After the loop, return the difference.*/

package lesson2.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class P6_04{

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int[] anArray = new int[10];
        for (int i = 0; i < anArray.length; i++) {
            System.out.printf("%d element of array: ", i);
            anArray[i] = input.nextInt();
        }
        input.close();
        System.out.println(sumWithoutSmallest(anArray));
    }


    public static int sumWithoutSmallest(int[] array){
        int min_value=0;
        int sum_array=0;

        for (int i=0; i< array.length;i++){
            sum_array+=array[i];

           if(  i==0) {
                min_value=array[i];
            }
            else {
                if (min_value>array[i]) { min_value=array[i];}
            }

        }
        System.out.println(min_value);
        System.out.println(sum_array);


        return sum_array-min_value;
    }

}

