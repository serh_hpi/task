/*Write a method
 public static boolean equals(int[] a, int[] b)
 that checks whether two arrays have the same elements in the same order.*/
package lesson2.arrays;

public class P6_09 {
    public static void main(String[] args) {
        int[] one = {1, 22, 3, 45, 6, 77, 8, 9, 44};
        int[] two = {1, 22, 3, 45, 6, 77, 8, 9, 44};

        System.out.println("Элементы в одном и том же порядке? " + equals(one, two));

    }

    public static boolean equals(int[] a, int[] b) {

        if (a == b)
            return true;

        if (a == null || b == null)
            return false;

       if (b.length != a.length)
            return false;

        for (int i = 0; i < a.length; i++)
            if (a[i] != b[i])
                return false;

        return true;
    }
}