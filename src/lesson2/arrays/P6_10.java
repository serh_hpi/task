/*Write a method
public static boolean sameSet(int[] a, int[] b)
that checks whether two arrays have the same elements in some order, ignoring
duplicates. For example, the two arrays
1  4  9  16  9  7  4  9  11
and
11  11  7  9  16  4  1
would be considered identical. You will probably need one or more helper methods.*/

package lesson2.arrays;

import java.util.Arrays;

public class P6_10 {
    public static void main(String[] args) {
       int[] one={1, 4, 9, 16, 9,  7,  4,  9,  11};
       int[] two={11,  11,  7,  9, 9, 16,  4,  1};


        System.out.println("Array one: " +Arrays.toString( delete_dublicates(one)));
        System.out.println("Array two: "+ Arrays.toString( delete_dublicates(two)));
        System.out.println("Arrays equals? "+ sameSet(delete_dublicates(one),delete_dublicates(two)));

    }

    public static boolean sameSet(int[] a, int[] b) {

        if (a == b)
            return true;

        if (a == null || b == null)
            return false;

        if (b.length != a.length)
            return false;

        for (int i = 0; i < a.length; i++)
            if (a[i] != b[i])
                return false;

        return true;

    }

    public static int[] delete_dublicates(int[] a) {

        int[] b = new int[ a.length];

        Arrays.sort(a);
        int pos=0;

        for (int i=0; i< a.length; i++) {

            if (i + 1 < a.length) {
                if (a[i] != a[i + 1]) {
                    b[pos]=a[i];
                    pos++;
                }
            }
            else {
                 b[pos]=a[i];
            }
        }
         return Arrays.copyOf(b,pos+1);
    }

}