/*
 Write a program that prompts the user for two integers and then prints
 •    The sum
 •    The difference
 •    The product
 •    The average
 •    The distance (absolute value of the difference)
 •    The maximum (the larger of the two)
 •    The minimum (the smaller of the two)
 Hint: The max and min functions are declared in the Math class.
*/
package lesson2.primitives;

import java.util.Scanner;

public class P2_04{
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Input first integer : ");
        int first = in.nextInt();

        System.out.print("Input second integer : ");
        int second =  in.nextInt();


        System.out.println("summ="+(first+second));
        System.out.println("difference="+(first-second));

        System.out.println("average="+ ((first+second)/2));
        System.out.println("distance="+ Math.abs(first-second));
        System.out.println("maximum="+ Math.max(first,second));
        System.out.println("minimum="+ Math.min(first,second));
    }

}