/*
Write a program that asks the user for the lengths
of the sides of a rectangle. Then print
•    The area and perimeter of the rectangle
•    The length of the diagonal (use the Pythagorean theorem)
*/
package lesson2.primitives;

import java.util.Scanner;

public class P2_08{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input height : ");
        double height = in.nextDouble();

        System.out.print("Input width : ");
        double width =  in.nextDouble();

        double Perimeter = (height + width) * 2;
        double Diagonal= Math.sqrt(Math.pow(height,2)+Math.pow(width,2));

        System.out.println("Perimeter="+Perimeter);
        System.out.println("Diagonal="+Diagonal);
    }
}